require 'rails_helper'

RSpec.describe RecipeService, type: :service do
    describe '#get_entries' do
        before do
            VCR.use_cassette('recipe_entries') do
                @recipe_entries =  RecipeService.get_entries
            end
        end

        it { expect(@recipe_entries.size).to eq(4) }

        it 'retrieve entry title and photo_url' do
            entry = @recipe_entries.first
            expect(entry.title).to_not be_nil
            expect(entry.photo.url).to_not be_nil
        end
    end

    describe '#get_details' do
        before do
            VCR.use_cassette('recipe_details') do
                id = '4dT8tcb6ukGSIg2YyuGEOm'
                @recipe_details = RecipeService.get_details(id)
            end
        end
        
        it { expect(@recipe_details.title).to eq('White Cheddar Grilled Cheese with Cherry Preserves & Basil') }
        it { expect(@recipe_details.description).to eq('*Grilled Cheese 101*: Use delicious cheese and good quality bread; make crunchy on the outside and ooey gooey on the inside; add one or two ingredients for a flavor punch! In this case, cherry preserves serve as a sweet contrast to cheddar cheese, and basil adds a light, refreshing note. Use __mayonnaise__ on the outside of the bread to achieve the ultimate, crispy, golden-brown __grilled cheese__. Cook, relax, and enjoy!') }
    end
end