# Recipes app

This app is using Rails I've used this framework because is the most famous Ruby framework and also is easily to prototype an application, but it has downsides like an unnecessary database connection for this app purpose. I could use Sinatra or other simpler framework.

I've used the layout inside the pages because Rails 6 uses webpacker and other frontend technologies that I'm not so used, so I didn't prioritize this over the other tasks.

It's necessary to configure the database (as a downside to use Rails). For the development, I've used a simple docker container with it running

```
docker run --rm --name pg-docker -e POSTGRES_PASSWORD=docker -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data postgres
```

For use the `contentful SDK` as they recommend in their own webpage (and it's well maintained) it's necessary to configure the `space` and `access_token` using the rails credential encryption like the following command:

```
EDITOR=vim bin/rails credentials:edit
```

This app could be dockerized for easier deploy, but since it's not exactly the scope of the challenge I didn't did that.

For run in production is necessary to first run 

```
bundle install
```

and them
```
rails s
```

(Not forgeting to configure the database)

To run the tests use

```
bundle exec rspec spec/
```

---
## Some apps screenshots:

### Recipe List
![image](https://user-images.githubusercontent.com/111924/97985896-31782c00-1dd9-11eb-8d8d-80f9ba267d66.png)

### Recipe Details
![image](https://user-images.githubusercontent.com/111924/97985915-3806a380-1dd9-11eb-8e41-8420f9296dae.png)