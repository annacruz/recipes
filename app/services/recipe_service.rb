# frozen_string_literal: true

require 'contentful'

class RecipeService
    def self.get_entries
       client.entries(content_type: 'recipe') 
    end

    def self.get_details(id)
        client.entry(id)
    end

    private
    def self.client
        Contentful::Client.new(
            space: Rails.application.credentials.contentful[:space], 
            access_token: Rails.application.credentials.contentful[:access_token]
        )
    end
end