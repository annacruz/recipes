class RecipesController < ApplicationController
  def index
    @recipes = RecipeService.get_entries
  end

  def show
    @recipe = RecipeService.get_details(params[:id])
  end

end
